
import params

import os, shutil, sys



#-------------------------------------------------------------------------------------------------------------
def CreateEnsemble(params):

    EnsembleParams = {}
    MemberParamDict = {}
    KeyList=sorted(params.ParamValues.keys())
    EnsembleParams = ProcessParams(params.ParamValues,0,KeyList,params.EnsemblePrefix,EnsembleParams,MemberParamDict,params.LinkedValues)

    return EnsembleParams

#-------------------------------------------------------------------------------------------------------------
def ProcessParams(ParamValues,ii,KeyList,prefix,EnsembleParams,MemberParamDict,LinkedValues):

    # ii counts over ParamValues entries, i.e. over parameters.
    ValueList = ParamValues[KeyList[ii]][2]
    for ValueIndex in range(len(ValueList)):
        Value = ValueList[ValueIndex]
        #print(ValueIndex,Value)
        MemberParamDict[str(KeyList[ii])] = Value
        LinkedParams = ParamValues[KeyList[ii]][3]
        if len(LinkedParams)>0:
            for LinkedParamName in LinkedParams:
                #print(LinkedParamName,LinkedValues[LinkedParamName][1][ValueIndex])
                MemberParamDict[LinkedParamName] = LinkedValues[LinkedParamName][1][ValueIndex]
            #print(prefix,ii,KeyList[ii],ParamValues[KeyList[ii]][1][1])
        if ii+1 < len(KeyList):
            if len(ValueList)>1:
                MemberPrefix = "_"+ParamValues[KeyList[ii]][0]+str(Value)
            else:
                MemberPrefix = ""
            ProcessParams(ParamValues,ii+1,KeyList,prefix+MemberPrefix,EnsembleParams,MemberParamDict,LinkedValues)
        elif ii+1 == len(KeyList):
            MemberName = prefix.replace(".","")
            EnsembleParams[MemberName] = MemberParamDict.copy()
            
    return EnsembleParams

#-------------------------------------------------------------------------------------------------------------
def CreateDirectories(params, EnsembleParams):

    #print("Making new root dir "+params.RootDir+"/"+params.EnsembleName)
    os.chdir(params.RootDir)
    try:
        os.mkdir(params.RootDir+"/"+params.EnsembleName)
    except:
        pass
        #print("cannot mkdir but will carry on anyway... ")
    os.chdir(params.RootDir+"/"+params.EnsembleName)

    for MemberName in EnsembleParams:
        #print("Making new dir "+MemberName)
        try:
            os.mkdir(MemberName)
        except:
            pass
            #print(" cannot mkdir but will carry on anyway... ")

        for Template in params.TemplateFiles:
            FileName = params.RootDir+"/"+params.TemplateFiles[Template]
            shutil.copy(FileName,MemberName+"/")

        for SubDir in params.RequiredSubDirectories:
            #print("Making new dir "+params.RootDir+"/"+params.EnsembleName+"/"+MemberName+"/"+SubDir)
            try:
                os.mkdir(params.RootDir+"/"+params.EnsembleName+"/"+MemberName+"/"+SubDir)
            except:
                pass
                #print(" cannot mkdir but will carry on anyway... ")

    return

#-------------------------------------------------------------------------------------------------------------
def SetParamValues(params, EnsembleParams):

    os.chdir(params.RootDir+"/"+params.EnsembleName)
    for MemberName in EnsembleParams:
        os.chdir(params.RootDir+"/"+params.EnsembleName+"/"+MemberName)
        MemberDict = EnsembleParams[MemberName]
        for ParamName in MemberDict:
            if ParamName in params.ParamValues:
                FileName = params.TemplateFiles[params.ParamValues[ParamName][1]]
            else:
                FileName = params.TemplateFiles[params.LinkedValues[ParamName][0]]
            InFile = params.RootDir+"/"+params.EnsembleName+"/"+MemberName+"/"+FileName
            OutFile = params.RootDir+"/"+params.EnsembleName+"/"+MemberName+"/"+FileName
            with open(InFile, 'r') as file:
                filedata = file.read()
            filedata = filedata.replace(ParamName,str(EnsembleParams[MemberName][ParamName]))
            with open(OutFile, 'w') as file:
                file.write(filedata)

        # for now we assume derived params are only for member name
        for DerivedParam in params.DerivedParams:
            FileName = params.TemplateFiles[params.DerivedParams[DerivedParam]]
            #InFile = params.RootDir+"/"+FileName
            InFile = params.RootDir+"/"+params.EnsembleName+"/"+MemberName+"/"+FileName
            OutFile = params.RootDir+"/"+params.EnsembleName+"/"+MemberName+"/"+FileName
            with open(InFile, 'r') as file:
                filedata = file.read()
            filedata = filedata.replace(DerivedParam,str(MemberName))
            with open(OutFile, 'w') as file:
                file.write(filedata)

            
    return


#-------------------------------------------------------------------------------------------------------------
def CreateLaunchScript(params,EnsembleParams):

    os.chdir(params.RootDir+"/"+params.EnsembleName+"/")

    with open(params.LaunchScript, 'w') as file:
        for MemberName in EnsembleParams:
            file.write("\ncd "+params.RootDir+"/"+params.EnsembleName+"/"+MemberName+"/")
            file.write("\nsbatch "+params.TemplateFiles["BatchFile"] )
        
    return
    
#-------------------------------------------------------------------------------------------------------------
def EnsembleLength(ParamValues):

    EnsLen = 1
    for key in ParamValues:
        EnsLen = EnsLen * len(ParamValues[key][2])
    return EnsLen


#-------------------------------------------------------------------------------------------------------------
if __name__ == "__main__":

    NumParams = len(params.ParamValues)
    EnsLen = EnsembleLength(params.ParamValues)    
    print ("Number of parameters is",NumParams)
    print ("Ensemble size will be",EnsLen)

    EnsembleParams = CreateEnsemble(params)
    CreateDirectories(params, EnsembleParams)
    SetParamValues(params, EnsembleParams)
    CreateLaunchScript(params,EnsembleParams)
    
