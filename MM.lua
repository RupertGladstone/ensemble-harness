
outdir = "./vtuoutputs"



meshdb  = ""
MESH_IN = "" 
MESH_OUT= ""
name    = "MM"

Hmin  = 40.0 -- ## H min (limiter threshold)
Hinit = 500.0

-- ## timestepping and accelerated forcing
dt  = XXX_dt/yearindays   
nt  = 200000                      -- ## number of timesteps
of  = 5000                        -- ## output frequency
sof = 100                         -- ## scalars output frequency
acceleration = XXX_Acceleration

nn = 3.0
ng = nn
mm = 1.0/nn

-- ## A given in MPa-3/a (2e-17*1e6^(-3) Pa-3/a)
AA  = 20.0
eta = (2.0*AA)^(-1.0/nn)

bmin = -Hinit*rhoi/rhoo

-- ## controlling steady state iterations
IMIN=10
IMAX=100

-- ## For Grounded solver
GLTolerance = 1.0e-5 

-- ## Inversion regularisation
Lambda=1.0e10

-- ## levels in the vertical
MLEV=15


-- ## Geometry functions for MISOMIP

function bedrock(xc,yc)
  Lx = 640000.0
  Ly = 80000.0
  B0 = -150.0
--  B2 = -728.8
--  B4 = 343.91
--  B6 = -50.57
  B2 = XXX_B2
  B4 = XXX_B4
  B6 = XXX_B6
  xt = 300000.0
  fc= 4000.0
  zbdeep = -1000.0
  dc = 500.0
  wc = 24000.0
  xcalve = 640000.0
  x = xc/xt
  y = yc
  aux1 = 1.0 + math.exp(-2.0*(y -0.5*Ly - wc)/fc)
  aux2 = 1.0 + math.exp(2.0*(y -0.5*Ly + wc)/fc)
  By = dc*(1/aux1 + 1/aux2)
  Bx = B0 + B2*x^2 + B4*x^4 + B6*x^6
  -- print(">>>>> x,y=",x,y,"Bx=",Bx,"By=",By)
  -- print(">>>>>>> x,y=",x,y,"aux", 1/aux1, 1/aux2)
  return max(Bx + By, zbdeep)
end

function bedrock2(xc,yc)
  Lx = 640000.0
  Ly = 80000.0
  B0 = -150.0
  B2 = -728.8
  B4 = 343.91
  B6 = -50.57
  xt = 300000.0
  fc= 4000.0
  zbdeep = -720.0
  dc = 500.0
  wc = 24000.0
  xcalve = 640000.0
  x = xc/xt
  y = yc
  aux1 = 1.0 + math.exp(-2.0*(y -0.5*Ly - wc)/fc)
  aux2 = 1.0 + math.exp( 2.0*(y -0.5*Ly + wc)/fc)
  By = dc*(1/aux1 + 1/aux2)
  Bx = B0 + B2*x^2 + B4*x^4 + B6*x^6
  -- print(">>>>> x,y=",x,y,"Bx=",Bx,"By=",By)
  -- print(">>>>>>> x,y=",x,y,"aux", 1/aux1, 1/aux2)
  return max(Bx + By, zbdeep)
end

function initialzb2(xc,yc)
  Hminb = Hmin*(1.0 - rhoi/rhosw) 
  if (xc < 200000.0) then
     zb =  min(bedrock(xc,yc), Hminb)
  elseif (xc < 250000.0)  then
     xrel = (xc - 200000.0)/50000.0
     -- print(">>>>  Hminb=",  Hminb, xrel, bedrock(xc,yc))
     zb = min(bedrock(200000.0,yc)*(1.0 - xrel) - Hminb*xrel, Hminb)
  else
    zb = Hminb
  end
  return zb
end

function initialzb(xc,yc)
  xs = 180000.0
  xe = 200000.0
  Hminb = Hmin*rhoi/rhosw
  xr = (xc - xs)/(xe - xs)  
  if (xr <= 0.0) then
    zb =  bedrock(xc,yc)
  elseif ((xr <= 1.0) and (xr > 0)) then   
    zb =  (1.0 - xr)*(bedrock(xc,yc) + Hminb) - Hminb    
  else
    zb = -Hminb
  end 
  return max(zb, bedrock(xc,yc))
end

function initialzs(xc,yc)
  xs = 180000.0
  xe = 200000.0
  Hminf = Hmin*(1.0 - rhoi/rhosw)
  Hfxs =  -rhosw*bedrock(xs,0.0)/rhoi + bedrock(xs,0.0) - Hminf
  xr = xc/xs
  zbref = bedrock(xc,40000.0)
  if ((xr < 1.0) and (xr >= 0.0)) then
    zs = 400.0*(1.0 - xr^2.0) + Hminf
  elseif ((xr >=1.0) and (xc < xe)) then
    newxr= (xc - xs)/(xe - xs)
    zs = Hfxs*(1.0 - newxr) + Hminf
  else   
    zs =  Hminf
  end  
  return max(zs,bedrock(xc,yc) + Hmin)
end




-- ## mesh refinement stuff below...

-- ## Tolerated errors on U and H
U_err=2
H_err=4.0

-- ## mesh size limits in different regions

-- ## absolute minimum mesh size
Mminfine=500.0

-- ## minimum mesh size far from grounding line (may be higher than
-- ## Mminfine to prevent detailed refinement in slow flowing far 
-- ## inland regions).  Set equal to Mminfine if you don't want this.
Mmincoarse=1000.0

-- ## Maximum mesh size far from the grounding line 15000
Mmaxfar=1500.0

-- ## Maximum mesh size close to the grounding line 2000
Mmaxclose=700.0

-- ## maximum mesh size for ice shelves (set this to larger than
-- ## Mmaxfar if you want it to be ignored)
Mmaxshelf=1500.0

-- ## reference velocity used in refinement to set upper limit on
-- ## element size (together with distance from GL).  Sections of
-- ## grounding line with flow speeds higher than this limit will
-- ## have max mesh size Mmaxclose.  Max mesh is allowed to be
-- ## coarser for sections of slower flowing GL.  Set this very
-- ## small (e.g. 0.1) if you want it to be ignored.
refvel = 700

-- ## The distance from grounding line at which the upper limit for
-- ## the maximum mesh size is reached
GLdistlim=300000.0

-- ## The distance from the boundary at which the upper limit for
-- ## the maximum mesh size is reached 80000
-- ##Bdistlim=70000.0
Bdistlim=50000.0

-- ## For distances beyond distlim, the minimum mesh size may be
-- ## increased towards Mmincoarse on this distance scale 400000
distscale=300000.0

