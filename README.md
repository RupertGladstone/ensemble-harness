
# Ensemble harness

Python scripts for setting up multiple simulations intended to be run on HPC resource.
Intention is for perturbed parameter ensembles.

Preliminary usage is for running multiple Elmer/Ice simulations on mahti.csc.fi.  
It will also be used on LUMI and potentially on any machine.

It works by making multiple copies of a small number of template files and replacing certain strings in said files with prescribed parameter values.

#### To use this harness on `mahti.csc.fi`

Edit the `params.py` file and run `python3 harness.py`

Then use the created launch script to submit the jobs (or do it manually if you don't want to launch everything at once!).


## Prescribing parameters

This refers to the variables, lists and dictionaries in the `params.py` file.	

A directory named by variable **EnsembleName**, containing all the files for running the ensemble, will be created in **RootDir**.
Within this, a subdirectory will be created for each ensemble member.

A file named by variable **LaunchScript** will also be created in this subdirectory, containing commands to enter each subdirectory in turn and use `sbatch` to launch the job.

The **RequiredSubDirectories** will be created as empty directories for each ensemble member.

**TemplateFiles** contains the files that need to be copied to the subdirectory in which an ensemble member will be run.
These files may contain parameters that will vary from member to member.
The keys are user friendly strings to describe the files and the values are the actual filenames.

**ParamValues** is a python dictionary containing information about the different values that can be used.
The keys indicate text that needs to be replaced with a value.
This replacement happens for each ensemble member in the copies of the template files.

Each value in ParamValues is a list containing the following items in order:
* A short abbreviation for the parameter name (to be used in constructing the ensemble member name).
* A string to indicate which file contains this parameter (this string must be a key for the TemplateFiles dictionary).
* A list of valid values for this parameter.
* A list of linked parameters (must match keys in the LinkedValues dictionary).


**LinkedValues** If multiple parameters have values that are linked, i.e. their values vary together rather than
independently, only one of the parameters should be included in the ParamValues dictionary, and the others
should be included in LinkedValues.

The keys are the strings to be replaced.

The values are length 2 lists containing:
* A string to indicate which file contains this parameter (this string must be a key for the TemplateFiles dictionary).
* A list of valid values for this parameter (length must match the number of values for the corresponding parameter in ParamValues).



**DerivedParams** indicates text in the template files that needs to be handled in a specific way.
In the first instance this is just used for giving a name to each ensemble member, for files (e.g. the Elmer sif)
that need to know their own name so that they can name their output files,
or, for the batch file, give the runing job a distinct name in SLURM.
These names are created from the **EnsemblePrefix** and string casting of the the specific parameter values
for each member.



## Extending usage for other applications

This currently works for hard-coded lists of parameter values and fully samples all possible combinations.
Extending this to sampling over prescribed distributions using sampling techniques such as
LHS will be implemented at some point.
This could be done by a new script that auto generates parts of the `params.py` file.
