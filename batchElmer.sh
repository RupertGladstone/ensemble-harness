#!/bin/bash -f
#SBATCH --partition=medium
#SBATCH --time=36:00:00
##SBATCH --partition=test
##SBATCH --time=00:15:00
#SBATCH --job-name=XXX_JobName
#SBATCH --output=output.txt
#SBATCH --error=error.txt
#SBATCH --nodes=XXX_nodes
#SBATCH --ntasks-per-node=128
#SBATCH --account=project_2000339

echo Ice1r_GlaDS_spinup.sif > ELMERSOLVER_STARTINFO

module load elmer/rmg
##module load elmer/rmg-gl

##export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$NETCDF_DIR/include/"
##export PATH="$PATH:$NETCDF_DIR/include/"

#elmerf90 USF_ExitCondition.F90 -o USF_ExitCondition.so
#elmerf90 USF_SSAViscosity.F90  -o USF_SSAViscosity.so
#elmerf90 UGridDataReader_t.F90 -o UGridDataReader.so -I /opt/cray/pe/netcdf/4.9.0.3/gnu/9.1/include/
#elmerf90 Weertman2Coulomb.F90 -o Weertman2Coulomb.so
#elmerf90 GlaDSCoupledSolver.F90 -o GLADS_local.so
#elmerf90 GlaDSchannelSolver.F90 -o GLADS_channel_local.so
#elmerf90 GMValid.F90 -o GMValid.so

which ElmerSolver

srun ElmerSolver
