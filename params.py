
RootDir        = "/scratch/project_2000339/gladston/MISOMIP_GlaDS"
EnsembleName   = "SpinUp2"
EnsemblePrefix = "Spin"
LaunchScript   = "LaunchEnsemble.sh"

TemplateFiles = {
    "BatchFile"   :"batchElmer.sh",
    "SolverInput" :"Ice1r_GlaDS_spinup.sif",
    "LUAparams"   :"MM.lua",
    "GlaDSparams" :"glads_param.lua",
    "LUAfunctions":"COLD.lua",
}

RequiredSubDirectories = [
    "vtuoutputs",
    "results",
    "MM_refinedMesh",
]

ParamValues = {
    "XXX_Acceleration":["acc", "LUAparams",   [50]                      ,[]],
    "XXX_dt":          ["dt",  "LUAparams",   [0.5]                     ,[]],
    "XXX_SMB":         ["smb", "SolverInput", [0.5,1.0]                 ,[]],
    "XXX_nodes":       ["nn",  "BatchFile",   [1]                       ,[]],
    "XXX_gcnrx":       ["rf",  "SolverInput", [0.8]                     ,[]],
    "XXX_B2":          ["bed", "LUAparams",   [-980.0]                  ,["XXX_B4","XXX_B6"]],
    "XXX_Friction":    ["As",  "SolverInput", [0.1,0.05]                ,[]],
    "XXX_SheetCon":    ["gsc", "GlaDSparams", [0.00015,0.00005]         ,[]],
    "XXX_EVratio":     ["gev", "GlaDSparams", [0.001]                   ,[]],
    "XXX_GTmelt":      ["gtm", "SolverInput", [0.0]                     ,[]],
    "XXX_ExecGlaDS":   ["hyd", "SolverInput", ["always"]                ,["XXX_ExecChannels"]],
}

# XXX_gcnrx is short for GlaDS coupled solver nonlinear relaxation factor
# XXX_GTmelt is short for GeoThermal melt, an additional baseline grounded melt that is added to the GlaDS volume source
#    "XXX_B2":          ["bed", "LUAparams",   [-728.8,-980.0]       ,["XXX_B4","XXX_B6"]],
#    "XXX_LatShelfSlip":["lss", "SolverInput", [0.1,1.0,10.0,100.0,1000.0]   ,[]],
#    "XXX_ExecGlaDS":   ["hyd", "SolverInput", ["always","never"]    ,["XXX_ExecChannels"]],
#    "XXX_GTmelt":      ["gtm", "SolverInput", [0.0,0.001,0.01]      ,[]],

LinkedValues = {
    "XXX_B4":           ["LUAparams",  [498.0 ]],
    "XXX_B6":           ["LUAparams",  [-74.00]],
    "XXX_ExecChannels": ["SolverInput",["After Saving"]]
}

#    "XXX_ExecChannels": ["SolverInput",["After Saving","never"]]
#    "XXX_B4": ["LUAparams",  [343.91, 498.0 ]],
#    "XXX_B6": ["LUAparams",  [-50.57, -74.00]],

DerivedParams = {
    "XXX_JobName":"BatchFile",
    "XXX_NameRun":"SolverInput",
}



